
const crypto = require('crypto'),
    Benchmark = require('benchmark'),
    sha1 = require('sha1'),
    suite = new Benchmark.Suite,
    createHash = crypto.createHash,
    testPasswordString = 'tip@p@ssw0rd!';

suite
    .add('sha1', function() {
        sha1(testPasswordString);
    })
    .add('crypto md5', function() {
        createHash('md5').update(testPasswordString).digest('hex');
    })
    .add('crypto sha1', function() {
        createHash('sha1').update(testPasswordString).digest('hex');
    })
    .add('crypto sha256', function() {
        createHash('sha256').update(testPasswordString).digest('hex');
    })
    .add('crypto sha512', function() {
        createHash('sha512').update(testPasswordString).digest('hex');
    })
    .on('cycle', function(event) {
        console.log(String(event.target));
    })
    .on('complete', function() {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    .run({ 'async': true });
